package start;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;
import presentationLayer.AdministratorGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

public class ControllerAdmin {
    private AdministratorGUI view1;

    public ControllerAdmin(AdministratorGUI ad){
        this.view1 = ad;

        this.view1.getCreate().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BaseProduct p = new BaseProduct(view1.getText1().getText(), Double.parseDouble(view1.getText2().getText()));
                view1.getRestaurant().createMenuItem(p);
                view1.updateTable();
                try{

                    RestaurantSerializator.serialize((Restaurant) view1.getRestaurant());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        this.view1.getCreate2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CompositeProduct p = new CompositeProduct(view1.getText3().getText(), view1.getSelected());
                view1.getRestaurant().createMenuItem(p);
                view1.updateTable();
                try{

                    RestaurantSerializator.serialize((Restaurant) view1.getRestaurant());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        this.view1.getView().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view1.updateTable();
            }
        });

        this.view1.getDelete().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view1.getRestaurant().deleteMenuItem(view1.getText4().getText());
                view1.updateTable();
                try{

                    RestaurantSerializator.serialize((Restaurant) view1.getRestaurant());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        });

        this.view1.getEdit().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view1.getRestaurant().editMenuItem(view1.getOneSelect(), Double.parseDouble(view1.getText5().getText()));
                view1.updateTable();
                try{

                    RestaurantSerializator.serialize((Restaurant) view1.getRestaurant());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
