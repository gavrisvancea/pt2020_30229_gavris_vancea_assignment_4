package start;

import businessLayer.BaseProduct;
import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;
import presentationLayer.WaiterGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

public class ControllerWaiter {

    private WaiterGUI view2;

    public ControllerWaiter(WaiterGUI gui){
        this.view2 = gui;

        view2.getViewItems().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view2.table();
            }
        });

        view2.getOrder().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Order o = new Order(Integer.parseInt(view2.getText1().getText()), view2.getText2().getText(), Integer.parseInt(view2.getText3().getText()));
                view2.getRestaurant().createNewOrder(o, view2.getSelected());
                view2.orderTable();
                try{

                    RestaurantSerializator.serialize((Restaurant) view2.getRestaurant());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        view2.getPrice().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view2.getPriceText().setText(null);
                view2.getPriceText().setEditable(true);
                double price = 0;
                for(Order i: view2.getRestaurant().getMap().keySet()) {
                    if (i.getOrderID() == Integer.parseInt(view2.getText4().getText())) {
                        price = view2.getRestaurant().computePrice(i);
                    }
                }
                view2.getPriceText().setText(Double.toString(price));
                try{

                    RestaurantSerializator.serialize((Restaurant) view2.getRestaurant());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        view2.getBill().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(Order i: view2.getRestaurant().getMap().keySet()) {
                    if (i.getOrderID() == Integer.parseInt(view2.getText5().getText())) {
                        view2.getRestaurant().generateBill(i);
                    }
                }
                view2.orderTable();
                try{

                    RestaurantSerializator.serialize((Restaurant) view2.getRestaurant());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

        view2.getView().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view2.orderTable();
            }
        });
    }

}
