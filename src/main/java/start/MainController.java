package start;

import businessLayer.BaseProduct;
import businessLayer.MenuItem;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;

import presentationLayer.AdministratorGUI;
import presentationLayer.StartGUI;
import presentationLayer.WaiterGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class MainController {

    private StartGUI start;

    public MainController(StartGUI gui){
        this.start = gui;

        start.getAdmin().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AdministratorGUI gui1 = new AdministratorGUI((Restaurant) start.getRestaurant());
                ControllerAdmin admin = new ControllerAdmin(gui1);

            }
        });

        start.getWaiter().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                WaiterGUI gui2 = new WaiterGUI((Restaurant)start.getRestaurant());
                ControllerWaiter waiter = new ControllerWaiter(gui2);
            }
        });
    }

    public static void main(String[] args) {
        Restaurant rest = new Restaurant();
        try {
            rest = RestaurantSerializator.deserialize(args[0]);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(rest);
        StartGUI start = new StartGUI(rest);

        MainController main = new MainController(start);

    }
}
