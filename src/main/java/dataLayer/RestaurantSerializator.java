package dataLayer;

import businessLayer.BaseProduct;
import businessLayer.MenuItem;
import businessLayer.Restaurant;

import java.io.*;

public class RestaurantSerializator {

    public static void serialize(Restaurant object) throws IOException {

        FileOutputStream file = new FileOutputStream ("restaurant.ser");
        ObjectOutputStream out = new ObjectOutputStream(file);
        out.writeObject(object); // Method for serialization of object
        out.close();
        file.close();
    }

    public static Restaurant deserialize(String filename) throws IOException, ClassNotFoundException {
        Restaurant object = new Restaurant(); // SerializableClass implements Serializable
        FileInputStream file = new FileInputStream(filename);
        ObjectInputStream in = new ObjectInputStream(file);
        object = (Restaurant) in.readObject(); // Method for deserialization of object
        in.close();
        file.close();
        return object;
    }
}
