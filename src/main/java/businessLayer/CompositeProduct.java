package businessLayer;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem {

    private ArrayList<MenuItem> ingredients;

    public CompositeProduct(String name, ArrayList<MenuItem> ingreds){
        super(name, 0);
        this.ingredients = ingreds;
        super.setPrice(this.computePrice());
    }

    public double computePrice(){
        double price = 0;
        for(MenuItem i: ingredients){
            price += i.getPrice();
        }
        return price;
    }
    public void addItem(MenuItem item){
        this.ingredients.add(item);
    }

    public void removeItem(MenuItem item){
        this.ingredients.remove(item);
    }
}
