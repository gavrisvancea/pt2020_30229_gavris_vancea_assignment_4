package businessLayer;

import java.io.Serializable;

public class MenuItem implements Serializable {

    private String name;
    private double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public MenuItem(String n, double p){
        this.name = n;
        this.price = p;
    }
    public double computePrice(){
        return this.price;
    }

    public String toString(){
        return name + " " + price;
    }
}
