package businessLayer;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class Order implements Serializable {

    private int orderID;
    private String date;
    private int table;

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    public Order(int id, String d, int t){
        this.orderID = id;
        this.date = d;
        this.table = t;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderID == order.orderID &&
                table == order.table &&
                date.equals(order.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderID, date, table);
    }
}
