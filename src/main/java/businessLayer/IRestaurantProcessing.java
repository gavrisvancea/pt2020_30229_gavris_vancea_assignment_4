package businessLayer;

import java.util.ArrayList;
import java.util.HashMap;

public interface IRestaurantProcessing {

    //administrator

    /**
     * @pre menuItem != null
     * @post for all Restaurant rest | getMenu().size() != 0
     * @param menuItem new MenuItem inserted in MenuList
     */
    void createMenuItem(MenuItem menuItem);

    /**
     * @pre name != null
     * @pre for all Restaurant rest | getMenu().size() != 0
     * @pre for all Restaurant rest | getMenu().get(i).getName().equals(name)
     * @param name name of MenuItem which will be deleted
     */
    void deleteMenuItem(String name);

    /**
     * @pre MenuItem exists in Restaurant Menu
     * @pre price != 0
     * @post for all Restaurant rest | getMenu().size() != 0
     * @param m MenuItem which will be edited
     * @param price new price for the MenuItem m
     */
    void editMenuItem(MenuItem m, double price);

    //waiter

    /**
     * @pre order != null
     * @pre m.size() != 0
     * @post for all Restaurant rest | (getMap != null)
     * @param order key for new Order
     * @param m value for the Order order
     */
    void createNewOrder(Order order, ArrayList<MenuItem> m);

    /**
     * @pre order != null
     * @pre for all Restaurant rest | getMap.get(order) != null
     * @post result != null
     * @param order for which is compute the total price
     * @return total price
     */
    double computePrice(Order order);

    /**
     * @pre order != null
     * @param order for which is generated the Bill
     */
    void generateBill(Order order);

    HashMap<Order, ArrayList<MenuItem>> getMap();
    ArrayList<MenuItem> getMenu();
}
