package businessLayer;

import dataLayer.RestaurantSerializator;
import presentationLayer.ChefGUI;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

/**
 * Restaurant class is used to execute main operations executed by a administrator or a waiter
 */
public class Restaurant extends Observable implements IRestaurantProcessing, Serializable {

    private ArrayList<MenuItem> menu;
    private HashMap<Order, ArrayList<MenuItem>> command;

    /**
     * Constructor for an object Restaurant which initialize the list of MenuItems,
     * the HashMap of orders and add as Observer a frame which inform a chef for an order
     */
    public Restaurant(){
        menu = new ArrayList<MenuItem>();
        command = new HashMap<Order, ArrayList<MenuItem>>();
        this.addObserver(new ChefGUI());
    }

    /**
     * add in list of MenuItems a new MenuItem
     * @param menuItem new MenuItem inserted in MenuList
     */
    public void createMenuItem(MenuItem menuItem) {
        this.menu.add(menuItem);
        assert(menu != null);
    }

    /**
     * delete a MenuItem from the list of MenuItems
     * @param name of MenuItem which will be deleted
     */
    public void deleteMenuItem(String name) {
        assert(menu != null);
        for(MenuItem m: this.menu){
            if(m.getName().equals(name)){
                this.menu.remove(m);
                break;
            }
        }
    }

    /**
     * edit the price for a MenuItem
     * @param m MenuItem which will be edited
     * @param price the new price for the MenuItem specified
     */
    public void editMenuItem(MenuItem m, double price) {
        assert(menu != null);
        for(MenuItem i: menu){
            if(m.getName().equals(i.getName())){
                i.setPrice(price);
                break;
            }
        }
    }

    /**
     * add a new Order in HashMap
     * @param order the key for Order placed
     * @param items list of MenuItems which are ordered
     */
    public void createNewOrder(Order order, ArrayList<MenuItem> items) {
        this.command.put(order, items);
        assert(command != null);
        ChefGUI chef = new ChefGUI();
        setChanged();
        notifyObservers();

    }

    /**
     * compute the price for an Order
     * @param order for which will be computed the total price
     * @return total price
     */
    public double computePrice(Order order) {
        double price = 0;
        assert(command != null);
        for(Order i: command.keySet()){
            if(i.getOrderID() == order.getOrderID()){
                assert(command.get(i) != null);
                for(MenuItem m: command.get(i)){
                    price += m.getPrice();
                }
            }
        }
        return price;
    }

    /**
     * generates a bill for an Order an delete the Order from the HashMap
     * @param order for which is created the bill
     */
    public void generateBill(Order order) {
        try {
            FileWriter bill = new FileWriter("bill.txt");
            bill.write("Bill number: ");
            bill.write(Integer.toString(order.getOrderID()));
            bill.write("\n");
            bill.write("Order table: ");
            bill.write(Integer.toString(order.getTable()));
            bill.write("\n Ordered items: \n");
            assert(command.get(order) != null);
            for(MenuItem i: command.get(order)){
                bill.write("\t");
                bill.write(i.toString());
                bill.write("\n");
            }
            bill.write("Total price: ");
            bill.write(Double.toString(this.computePrice(order)));
            bill.write("\n Date: ");
            bill.write(order.getDate());
            bill.write("\n Thank you");
            bill.close();

            command.remove(order);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public HashMap<Order, ArrayList<MenuItem>> getMap(){
        return this.command;
    }

    public ArrayList<MenuItem> getMenu() {
        return menu;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "menu=" + menu +
                ", command=" + command +
                '}';
    }
}
