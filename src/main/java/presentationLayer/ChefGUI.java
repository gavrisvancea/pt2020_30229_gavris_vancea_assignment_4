package presentationLayer;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class ChefGUI implements Observer {

    private JFrame frame;

    public ChefGUI(){
        frame = new JFrame("Chef");
    }

    @Override
    public void update(Observable o, Object arg) {
        JOptionPane.showMessageDialog(frame, "Chef is preparing food!", "Chef", JOptionPane.PLAIN_MESSAGE, null);
    }
}
