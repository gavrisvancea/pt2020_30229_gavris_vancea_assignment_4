package presentationLayer;

import businessLayer.IRestaurantProcessing;
import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class WaiterGUI {

    private IRestaurantProcessing restaurant;
    private JFrame frame = new JFrame("Waiter");
    private Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    private JPanel panel = new JPanel();

    private JLabel label1 = new JLabel("Order ID: ");
    private JTextArea text1 = new JTextArea();

    private JLabel label2 = new JLabel("Date: ");
    private JTextArea text2 = new JTextArea();

    private JLabel label3 = new JLabel("Table NO: ");
    private JTextArea text3 = new JTextArea();

    private JTable dataTable;
    private JTable orderTable;

    private JButton viewItems = new JButton("View");

    private JButton order = new JButton("Order");

    private JLabel label4 = new JLabel("Compute price for order: ");
    private JTextArea text4 = new JTextArea();
    private JTextArea priceText = new JTextArea();
    private JButton price = new JButton("Price");

    private JLabel label5 = new JLabel("Generate bill for order: ");
    private JTextArea text5 = new JTextArea();
    private JButton bill = new JButton("Bill");

    private JButton view = new JButton("ViewOrders");
    public WaiterGUI(Restaurant restaurant){

        this.restaurant = restaurant;

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(725, 450);
        int x = (int) ((d.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((d.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
        panel.setLayout(null);

        panel.add(label1);
        label1.setBounds(10, 10, 80, 20);

        panel.add(text1);
        text1.setBounds(100, 10, 80, 20);

        panel.add(label2);
        label2.setBounds(10, 40, 80, 20);

        panel.add(text2);
        text2.setBounds(100, 40, 80, 20);

        panel.add(label3);
        label3.setBounds(10, 70, 80, 20);

        panel.add(text3);
        text3.setBounds(100, 70, 80, 20);

        this.dataTable = new JTable();
        panel.add(dataTable);
        dataTable.setBounds(10, 100, 225, 200);

        this.orderTable = new JTable();
        panel.add(orderTable);
        orderTable.setBounds(290, 10, 400, 250);

        panel.add(label4);
        label4.setBounds(290, 270, 150, 20);

        panel.add(text4);
        text4.setBounds(450, 270, 100, 20);

        panel.add(price);
        price.setBounds(560, 270, 80, 20);

        panel.add(priceText);
        priceText.setBounds(640, 270, 60, 20);

        panel.add(viewItems);
        viewItems.setBounds(10, 310, 70, 20);

        panel.add(order);
        order.setBounds(200, 30, 80, 40);

        panel.add(label5);
        label5.setBounds(290, 300, 150, 20);

        panel.add(text5);
        text5.setBounds(450, 300, 100, 20);

        panel.add(bill);
        bill.setBounds(560, 300, 80, 20);

        panel.add(view);
        view.setBounds(290, 330, 120, 80);

        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    public void table(){
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    panel.remove(dataTable);
                    try {
                        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                        ex.printStackTrace();
                    }
                    DefaultTableModel model = new DefaultTableModel(){
                        public Class<?> getColumnClass(int columnIndex) {
                            Class clazz = String.class;
                            switch (columnIndex) {
                                case 0:
                                    clazz = String.class;
                                    break;
                                case 1:
                                    clazz = Double.class;
                                    break;
                                case 2:
                                    clazz = Boolean.class;
                            }
                            return clazz;
                        }
                    };
                    model.addColumn("MenuItem");
                    model.addColumn("Price");
                    model.addColumn("Select");
                    for(int i = 0; i < restaurant.getMenu().size(); i++){
                        model.addRow(new Object[]{restaurant.getMenu().get(i).getName(), restaurant.getMenu().get(i).getPrice(), false});
                    }
                    dataTable = new JTable(model);
                    panel.add(dataTable);
                    dataTable.setBounds(10, 100, 225, 200);
                }
            });
    }

    public ArrayList<businessLayer.MenuItem> getSelected(){
        ArrayList<businessLayer.MenuItem> list = new ArrayList<businessLayer.MenuItem>();

        for(int j = 0; j < dataTable.getRowCount(); j++){
            Boolean checked = (Boolean) dataTable.getValueAt(j, 2);
            if(checked == true){
                list.add(new MenuItem((String)dataTable.getValueAt(j,0), (Double)dataTable.getValueAt(j, 1)));
            }
        }
        return list;
    }

    public void orderTable(){

        DefaultTableModel model = new DefaultTableModel();

        model.addColumn("OrderID");
        model.addColumn("Date");
        model.addColumn("Table");
        model.addColumn("MenuItems");
        model.insertRow(0, new Object[]{"OrderID", "Date", "Table", "MenuItems"});
        int i = 1;
        for(Order o: restaurant.getMap().keySet()){
            model.insertRow(i, new Object[]{o.getOrderID(), o.getDate(), o.getTable(), restaurant.getMap().get(o).toString()});
            i++;
        }

        orderTable = new JTable(model);


        panel.add(orderTable);
        orderTable.setBounds(290, 10, 400, 250);
        orderTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        orderTable.getColumnModel().getColumn(3).setPreferredWidth(200);
    }
    public JButton getViewItems() {
        return viewItems;
    }

    public JButton getOrder() {
        return order;
    }

    public JTextArea getText1() {
        return text1;
    }

    public JTextArea getText2() {
        return text2;
    }

    public JTextArea getText3() {
        return text3;
    }

    public JTextArea getText4() {
        return text4;
    }

    public JButton getPrice() {
        return price;
    }

    public JTextArea getPriceText() {
        return priceText;
    }

    public JButton getBill() {
        return bill;
    }

    public JTextArea getText5() {
        return text5;
    }

    public JButton getView() {
        return view;
    }

    public IRestaurantProcessing getRestaurant() {
        return restaurant;
    }
}
