package presentationLayer;

import businessLayer.IRestaurantProcessing;
import businessLayer.Restaurant;

import javax.swing.*;
import java.awt.*;

public class StartGUI {

    IRestaurantProcessing restaurant;
    private JFrame frame = new JFrame("Restaurant");
    private Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    private JPanel panel = new JPanel();

    private JLabel label1 = new JLabel("Welcome to my restaurant!");

    private JButton admin = new JButton("Administrator");
    private JButton waiter = new JButton("Waiter");

    public StartGUI(Restaurant restaurant){

        this.restaurant = restaurant;

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 200);
        int x = (int) ((d.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((d.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
        panel.setLayout(null);

        panel.add(label1);
        label1.setBounds(80, 10, 300, 50);
        Font font = new Font("Attr", Font.PLAIN, 20);
        label1.setFont(font);

        panel.add(admin);
        admin.setBounds(50, 70, 130, 30);

        panel.add(waiter);
        waiter.setBounds(200, 70, 130, 30);

        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    public JButton getAdmin() {
        return admin;
    }

    public JButton getWaiter() {
        return waiter;
    }

    public IRestaurantProcessing getRestaurant() {
        return restaurant;
    }
}
