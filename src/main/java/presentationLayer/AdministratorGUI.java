package presentationLayer;

import businessLayer.BaseProduct;
import businessLayer.IRestaurantProcessing;
import businessLayer.MenuItem;
import businessLayer.Restaurant;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AdministratorGUI {

    private IRestaurantProcessing restaurant;

    private JFrame frame = new JFrame("Administrator");
    private Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    private JPanel panel = new JPanel();
    private JLabel label1 = new JLabel("MenuItem name:");
    private JTextArea text1 = new JTextArea();
    private JLabel label2 = new JLabel("MenuItem price:");
    private JTextArea text2 = new JTextArea();
    private JLabel label3 = new JLabel("CompositeProduct name:");
    private JTextArea text3 = new JTextArea();
    private JLabel label4 = new JLabel("MenuItem name:");
    private JTextArea text4 = new JTextArea();
    private JLabel label5 = new JLabel("MenuItem price:");
    private JTextArea text5 = new JTextArea();
    private JButton create = new JButton("Create BaseProduct");
    private JButton create2 = new JButton("Create CompositeProduct");
    private JButton delete = new JButton("Delete MenuItem");
    private JButton edit = new JButton("Edit MenuItem");
    private JButton view = new JButton("View");
    private JTable dataTable;

    public AdministratorGUI(Restaurant restaurant){

        this.restaurant = restaurant;

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(625, 450);
        int x = (int) ((d.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((d.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
        panel.setLayout(null);

        panel.add(label1);
        label1.setBounds(10, 20, 120, 20);

        panel.add(text1);
        text1.setBounds(125, 20, 80, 20);

        panel.add(label2);
        label2.setBounds(10, 45, 120, 20);

        panel.add(text2);
        text2.setBounds(125, 45, 80, 20);

        panel.add(label3);
        label3.setBounds(200, 320, 150, 20);

        panel.add(text3);
        text3.setBounds(360, 320, 140, 20);

        dataTable = new JTable();
        panel.add(dataTable);
        dataTable.setBounds(350, 10, 275, 300);

        panel.add(create);
        create.setBounds(10, 80, 160, 30);

        panel.add(label4);
        label4.setBounds(10, 120, 120, 20);

        panel.add(text4);
        text4.setBounds(130, 120, 80, 20);

        panel.add(create2);
        create2.setBounds(350, 350, 200, 30);

        panel.add(delete);
        delete.setBounds(50, 150, 150, 30);

        panel.add(label5);
        label5.setBounds(10, 190, 120, 20);

        panel.add(text5);
        text5.setBounds(130, 190, 80, 20);

        panel.add(edit);
        edit.setBounds(50, 220, 150, 30);

        panel.add(view);
        view.setBounds(20, 320, 80, 80);
        view.setBackground(Color.GREEN);

        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    public void updateTable(){
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                panel.remove(dataTable);
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                    ex.printStackTrace();
                }
                DefaultTableModel model = new DefaultTableModel(){
                    public Class<?> getColumnClass(int columnIndex) {
                        Class clazz = String.class;
                        switch (columnIndex) {
                            case 0:
                                clazz = String.class;
                                break;
                            case 1:
                                clazz = Double.class;
                                break;
                            case 2:
                                clazz = Boolean.class;
                        }
                        return clazz;
                    }
                };
                model.addColumn("MenuItem");
                model.addColumn("Price");
                model.addColumn("Select");
                for(int i = 0; i < restaurant.getMenu().size(); i++){
                    model.addRow(new Object[]{restaurant.getMenu().get(i).getName(), restaurant.getMenu().get(i).getPrice(), false});
                }
                dataTable = new JTable(model);
                panel.add(dataTable);
                dataTable.setBounds(350, 10, 275, 300);
            }
        });
    }

    public ArrayList<MenuItem> getSelected(){
        ArrayList<MenuItem> list = new ArrayList<MenuItem>();

        for(int j = 0; j < dataTable.getRowCount(); j++){
            Boolean checked = (Boolean) dataTable.getValueAt(j, 2);
            if(checked == true){
                list.add(new MenuItem((String)dataTable.getValueAt(j,0), (Double)dataTable.getValueAt(j, 1)));
            }
        }
        return list;
    }

    public MenuItem getOneSelect(){

        ArrayList<MenuItem> list = new ArrayList<MenuItem>();
        for(int j = 0; j < dataTable.getRowCount(); j++){
            Boolean checked = (Boolean) dataTable.getValueAt(j, 2);
            if(checked == true){
                list.add(new MenuItem((String)dataTable.getValueAt(j,0), (Double)dataTable.getValueAt(j, 1)));
            }
        }
        return list.get(0);
    }

    public JButton getCreate2() {
        return create2;
    }

    public JButton getCreate(){
        return this.create;
    }

    public JButton getDelete() {
        return delete;
    }

    public JButton getEdit() {
        return edit;
    }

    public JTextArea getText1() {
        return text1;
    }

    public JTextArea getText2() {
        return text2;
    }

    public JTextArea getText3(){
        return text3;
    }

    public IRestaurantProcessing getRestaurant() {
        return restaurant;
    }

    public JButton getView() {
        return view;
    }

    public JTextArea getText4() {
        return text4;
    }

    public JTextArea getText5() {
        return text5;
    }

}
